﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using MakeupStudio.Api.Responses;

namespace MakeupStudio.Api.Attributes
{
    /// <summary>
    ///     Фильтр, который делает стандартные проверки модели
    ///     В случае ошибки возвращает BaseApiResponse
    ///     Справедлив для методов апи с методо post,put - где функция принимает один параметр запрос
    /// </summary>
    public class ValidateRequestAttribute : ActionFilterAttribute
    {
        /// <summary>
        ///  Выполняется перед выполенением экшена
        /// </summary>
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            Validate(actionContext);
            base.OnActionExecuting(actionContext);
        }

        /// <summary>
        /// Функция валидации запроса
        /// </summary>
        private void Validate(HttpActionContext actionContext)
        {
            var actionArguments = actionContext.ActionArguments;

            //if (actionArguments.Count == 1)
            //{
            //    var request = actionArguments.First();
            //    if (request.Value == null)
            //    {
            //        actionContext.ModelState.AddModelError("", "Empty request");
            //    }
            //}

            if (actionContext.ModelState.IsValid) return;

            var responseObj = new EmptyResponse()
            {
                ErrorCode = (int) HttpStatusCode.BadRequest,
                ErrorMessage = actionContext.ModelState.Values.SelectMany(e => e.Errors).FirstOrDefault()?.ErrorMessage
            };

            var response = actionContext.Request.CreateErrorResponse(HttpStatusCode.BadRequest, "bad request");
            response.Content = new ObjectContent(typeof(EmptyResponse), responseObj, new JsonMediaTypeFormatter());
            actionContext.Response = response;
        }
    }
}