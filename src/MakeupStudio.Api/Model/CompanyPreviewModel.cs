﻿using System;

namespace MakeupStudio.Api.Model
{
    /// <summary>
    /// Респонс с превью информацией о фирме
    /// </summary>
    public class CompanyPreviewModel
    {
        /// <summary>
        /// Id фирмы
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Название фирмы
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Минимальная цена на услугу
        /// </summary>
        public decimal MinPrice { get; set; }

        /// <summary>
        /// Ссылки на фотографии
        /// </summary>
        public string[] Photos { get; set; }

        /// <summary>
        /// Количество фотографий
        /// </summary>
        public int PhotosCount { get; set; }

        /// <summary>
        /// Аватарка фирмы
        /// </summary>
        public string Avatar { get; set; }
    }
}