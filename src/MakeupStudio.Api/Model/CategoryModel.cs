﻿namespace MakeupStudio.Api.Model
{
    /// <summary>
    /// Модель для отображения категории
    /// </summary>
    public class CategoryModel
    {
        public int CategoryId { get; set; }

        public string CategoryName { get; set; }

        /// <summary>
        /// Есть ли в данной категории услуги. Если есть услуги в категории, 
        /// значит дочерних категорий нет
        /// </summary>
        public bool HasWorks { get; set; }

        /// <summary>
        /// Дочерние категории
        /// </summary>
        public CategoryModel[] ChildCategories { get; set; }

        /// <summary>
        /// Услуги категории
        /// </summary>
        public WorkModel[] Works { get; set; }
    }
}