﻿namespace MakeupStudio.Api.Responses.Works
{
    public class PriceLimitsResponse : BaseResponse
    {
        /// <summary>
        /// Минимально возможная цена
        /// </summary>
        public decimal MinPrice { get; set; }

        /// <summary>
        /// Максимально возможная цена
        /// </summary>
        public decimal MaxPrice { get; set; }
    }
}