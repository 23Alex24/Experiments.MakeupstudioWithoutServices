﻿using System.Threading.Tasks;
using MakeupStudio.Api.Model;
using MakeupStudio.Api.Responses.Companies;
using MakeupStudio.Common.Utils.Mapping;
using MakeupStudio.Core;
using MakeupStudio.Core.Entities;

namespace MakeupStudio.Api.ResponseBuilders.Companies
{
    /// <summary>
    /// Билдер для респонса GetCompaniesForMapResonse (получение списка фирм для карты)
    /// </summary>
    public class GetCompaniesForMapResponseBuilder
    {
        private readonly IMapperUtility _mapperUtility;
        private readonly IUnitOfWork _unitOfWork;

        public GetCompaniesForMapResponseBuilder(IMapperUtility mapperUtility, IUnitOfWork unitOfWork)
        {
            _mapperUtility = mapperUtility;
            _unitOfWork = unitOfWork;
        }

        public async Task<GetCompaniesForMapResponse> Build()
        {
            var response = new GetCompaniesForMapResponse();
            var companies = await _unitOfWork.CompanyRepository.GetActiveCompanies();

            if (companies.Length > 0)
                response.Companies = _mapperUtility.Map<Company[], CompanyInfoModel[]>(companies);

            return response;
        }
    }
}