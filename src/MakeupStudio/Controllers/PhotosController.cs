﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MakeupStudio.Attributes;
using MakeupStudio.Business.Commands;
using MakeupStudio.Business.Commands.Photos;
using MakeupStudio.Common.Constants;
using MakeupStudio.Common.Utils.FileSystem;
using MakeupStudio.Common.Utils.Photos;
using MakeupStudio.Extensions;
using MakeupStudio.ModelBuilders;
using MakeupStudio.Requests;
using MakeupStudio.Responses;

namespace MakeupStudio.Controllers
{
    /// <summary>
    /// Контроллер для работы с фотографиями
    /// </summary>
    [Authorize]
    public class PhotosController : BaseController
    {
        private readonly IFileSystemUtility _fileSystemUtility;
        private readonly IPhotosUtility _photosUtility;

        public PhotosController(
            IFileSystemUtility fileSystemUtility, 
            IPhotosUtility photosUtility)
        {
            _fileSystemUtility = fileSystemUtility;
            _photosUtility = photosUtility;
        }


        /// <summary>
        /// Страница с фотографиями и аватарками
        /// </summary>
        public async Task<ActionResult> Index()
        {
            var builder = DependencyResolver.Current.GetService<PhotosIndexModelBuilder>();
            return View(await builder.Build(CurrentUser, GetBaseUrl()));
        }


        #region Изменение аватарки

        /// <summary>
        /// Страница изменения аватарки
        /// </summary>
        [HttpGet]
        public ActionResult EditAvatar()
        {
            return View();
        }

        /// <summary>
        /// Метод сохранения изменения аватарки
        /// </summary>
        [HttpPost]
        public async Task<ActionResult> EditAvatar(EditAvatarRequest request)
        {
            if (request == null)
            {
                SetErrorMessage(ValidationErrors.CHOOSE_FILE);
                return View();
            }

            if (!ModelState.IsValid)
            {
                SetErrorMessage(ModelState.GetFirstError());
                return View();
            }
                
            try
            {
                var path = SaveInTempFolder(request.Avatar);
                var arguments = CreateCommandArgs<ChangeAvatarCommandArgs>();
                arguments.TempAvatarPath = path;

                var commandResult = await ExecuteCommand<ChangeAvatarCommandArgs, EmptyCommandResult>(arguments);
                SetMessage(commandResult, "Аватарка успешно изменена");

                if (commandResult.IsSuccess)
                    return RedirectToAction("Index", "Photos");

                return View();
            }
            catch (Exception ex)
            {
                SetErrorMessage("Внутренняя ошибка сервера. Попробуйте еще раз");
                return View();
            }
        }

        #endregion


        #region Добавление фотографии

        /// <summary>
        /// Страница добавления фотографий
        /// </summary>
        [HttpGet]
        public ActionResult AddPhoto()
        {
            return View();
        }

        /// <summary>
        /// Метод для добавления фотографии
        /// </summary>
        [HttpPost]
        public async Task<ActionResult> AddPhoto(AddPhotoRequest request)
        {
            if (request == null)
            {
                SetErrorMessage(ValidationErrors.CHOOSE_FILE);
                return View();
            }

            if (!ModelState.IsValid)
            {
                SetErrorMessage(ModelState.GetFirstError());
                return View();
            }
                        
            try
            {
                var path = SaveInTempFolder(request.Photo);
                var arguments = CreateCommandArgs<AddPhotoCommandArgs>();
                arguments.TempPhotoPath = path;
                var commandResult = await ExecuteCommand<AddPhotoCommandArgs, EmptyCommandResult>(arguments);
               SetMessage(commandResult, "Фотография успешно добавлена");

                if (commandResult.IsSuccess)
                    return RedirectToAction("Index", "Photos");

                return View();
            }
            catch (Exception ex)
            {
                SetErrorMessage("Внутренняя ошибка сервера. Попробуйте еще раз");
                return View();
            }
        }

        #endregion


        #region Удаление фотографии

        /// <summary>
        /// Метод для удаления фотографии
        /// </summary>
        [HttpPost, ValidateRequest]
        public async Task<JsonResult> DeletePhoto(DeletePhotoRequest request)
        {
            var commandArgs = CreateCommandArgs<DeletePhotoCommandArgs>();
            commandArgs.PhotoId = request.PhotoId;

            var commandResult = await ExecuteCommand<DeletePhotoCommandArgs, EmptyCommandResult>(commandArgs);
            SetMessage(commandResult, "Фотография успешно удалена");
            var response = commandResult.ConverToResponse<EmptyResponse>();
            return Json(response);
        }

        #endregion


        #region Вспомогательные методы

        private string SaveInTempFolder(HttpPostedFileBase file)
        {
            var sourceFileName = file.FileName;
            var newFileName = _photosUtility.GenerateFileName(new FileInfo(sourceFileName).Extension);
            var tempPath = _fileSystemUtility.CombinePath(_fileSystemUtility.TempDirectory, newFileName);
            file.SaveAs(tempPath);
            return tempPath;
        }

        #endregion
    }
}