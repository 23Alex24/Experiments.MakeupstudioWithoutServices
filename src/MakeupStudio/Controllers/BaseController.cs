﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Mvc;
using MakeupStudio.Business.Commands;
using MakeupStudio.Common.Constants;
using MakeupStudio.Core.Entities;
using MakeupStudio.Extensions;
using MakeupStudio.Helpers;

namespace MakeupStudio.Controllers
{
    /// <summary>
    /// Базовый контроллер
    /// </summary>
    public abstract class BaseController : Controller
    {
        /// <summary>
        /// Текущий пользователь
        /// </summary>
        protected virtual Account CurrentUser
        {
            get
            {
                var result = new Account();
                var id = HttpContext.GetIntClaimsValue(ClaimTypes.NameIdentifier);
                if (!id.HasValue)
                    return null;

                result.Id = id.Value;
                result.Email = HttpContext.GetStringClaimsValue(ClaimTypes.Email);                
                var companyId = HttpContext.GetGuidClaimsValue(CustomClaimsConstants.COMPANY_ID);
                result.CompanyId = companyId ?? default(Guid);
                result.Name = HttpContext.GetStringClaimsValue(ClaimTypes.Name);
                result.Phone = HttpContext.GetStringClaimsValue(ClaimTypes.MobilePhone);

                var state = HttpContext.GetEnumValue<AccountState>(CustomClaimsConstants.ACCOUNT_STATE);
                result.AccountState = state ?? AccountState.Undefined;
                return result;
            }
        }

        /// <summary>
        /// Создает класс аргументов команды и заполняет текущего юзера
        /// </summary>
        /// <typeparam name="TCommandArgs">Тип аргументов команды, который нужно создать</typeparam>
        protected virtual TCommandArgs CreateCommandArgs<TCommandArgs>()
            where TCommandArgs : BaseCommandArguments, new()
        {
            var result = new TCommandArgs();
            result.CurrentUser = CurrentUser;
            return result;
        }

        /// <summary>
        /// Создает команду и выполняет ее
        /// </summary>
        /// <typeparam name="TArgs">Тип аргументов команды</typeparam>
        /// <typeparam name="TResult">Тип результата команды</typeparam>
        /// <param name="commandArgs">Аргументы для команды</param>
        protected virtual async Task<TResult> ExecuteCommand<TArgs, TResult>(TArgs commandArgs)
            where TArgs: class, ICommandArguments
            where TResult : class, ICommandResult, new()
        {
            var command = DependencyResolver.Current.GetService<ICommand<TArgs, TResult>>();
            return await command.Execute(commandArgs);
        }

        /// <summary>
        /// Переопределяет метод Json, чтобы он сериализовал с помощью NewtonSoft, а не стандартного сериализатора
        /// </summary>
        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding)
        {
            return new NewtonsoftJsonResult
            {
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                Data = data
            };
        }

        /// <summary>
        /// Переопределяет метод Json, чтобы он сериализовал с помощью NewtonSoft, а не стандартного сериализатора
        /// </summary>
        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new NewtonsoftJsonResult
            {
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                Data = data,
                JsonRequestBehavior = behavior
            };
        }

        /// <summary>
        /// Возвращает базовый адрес сайта
        /// </summary>
        protected string GetBaseUrl()
        {
            Uri url = new Uri(Request.Url.AbsoluteUri);
            return url.GetLeftPart(UriPartial.Authority);
        }

        /// <summary>
        /// Устанавливает сообщение об ошибке или успешное сообщение в зависимости от результата выполнения команды
        /// </summary>
        /// <param name="commandResult">Результат выполнения команды</param>
        /// <param name="successMessage">Сообщение, которое нужно показать если команда успешно выполнена</param>
        protected virtual void SetMessage(ICommandResult commandResult, string successMessage)
        {
            if (!commandResult.IsSuccess)
            {
                SetErrorMessage(commandResult.ErrorMessage);
                return;
            }

            if (!string.IsNullOrWhiteSpace(successMessage))
                SetSuccessMessage(successMessage);
        }

        /// <summary>
        /// Устанавливает в TempData сообщение об ошибке
        /// </summary>
        /// <param name="message">Текст ошибки</param>
        protected virtual void SetErrorMessage(string message)
        {
            if (!string.IsNullOrWhiteSpace(message))
                TempData[KeysConstants.TempData.ERROR_MESSAGE] = message;
        }

        /// <summary>
        /// Устанавливает в TempData успешное сообщение
        /// </summary>
        /// <param name="message">Текст сообщения</param>
        protected virtual void SetSuccessMessage(string message)
        {
            if (!string.IsNullOrWhiteSpace(message))
                TempData[KeysConstants.TempData.SUCCESS_MESSAGE] = message;
        }
    }
}