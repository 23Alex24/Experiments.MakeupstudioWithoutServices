﻿$(document).ready(function () {

    //=============================================================
    //=============================================================
    // Константы
    //=============================================================
    //=============================================================    
    var $saveBtn = $("#saveBtn");
    var $workNameTxt = $("#work-name");   
    var $workPrice = $("#work-price");
    var $errorContainer = $("#error-msg-container");
    var $errorText = $("#error-msg");
    var $workIdHidden = $("#WorkId");



    //=============================================================
    //=============================================================
    //Методы для работы с контролами
    //=============================================================
    //=============================================================

    //При выборе системного имени
    //
    function workName_OnClickSystemWorkName() {
        $workNameTxt.val($(this).text());
    }

    //Обработчик события нажатия кнопки сохранить
    //
    function btnSave_OnClick() {
        _hideError();
        _editWork();
    }

    //=============================================================
    //=============================================================
    // AJAX методы и вспомогательные методы
    //=============================================================
    //=============================================================

    //ajax метод для изменения услуги
    //
    function _editWork() {
        $.ajax({
            type: "POST",
            url: "/Works/EditWork",
            data: {
                WorkId: $workIdHidden.val(),
                WorkName: $workNameTxt.val(),
                Price: $workPrice.val()
            },
            success: function (data) {
                if (data.ErrorCode && data.ErrorCode != 0) {
                    _showError(data.ErrorMessage);
                } else {
                    location.href = "/Works";
                }
            },
            error: function (data) {
                _showError(data);
            }
        });
    }

    //Отображает ошибку
    //
    function _showError(errorMsg) {
        $errorText.text(errorMsg);
        $errorContainer.show();
    }

    //Скрывает ошибку
    //
    function _hideError() {
        $errorContainer.hide();
    }



    //=============================================================
    //=============================================================
    //INIT
    //=============================================================
    //=============================================================
    function init() {               
        $saveBtn.click(btnSave_OnClick);
        $(".system-work-name").click(workName_OnClickSystemWorkName);
    }

    init();
});