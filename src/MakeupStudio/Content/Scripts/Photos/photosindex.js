﻿$(document).ready(function () {
    //=============================================================
    //=============================================================
    // Константы
    //=============================================================
    //=============================================================       
    var $errorContainer = $("#error-msg-container");
    var $errorText = $("#error-msg");


    //=============================================================
    //=============================================================
    //Методы для работы с контролами
    //=============================================================
    //=============================================================

    //Отображает ошибку
    //
    function _showError(errorMsg) {
        $errorText.text(errorMsg);
        $errorContainer.show();
    }

    //Скрывает ошибку
    //
    function _hideError() {
        $errorContainer.hide();
    }

    $(".remove-photo").click(function () {
        _hideError();

        if (confirm("Вы действительно хотите удалить фотографию?")) {
            var photoId = $(this).data("id");

            $.ajax({
                type: "POST",
                url: "/Photos/DeletePhoto",
                data: {
                    PhotoId: photoId
                },
                success: function (data) {
                    if (data.ErrorCode && data.ErrorCode != 0) {
                        _showError(data.ErrorMessage);
                    } else {
                        location.reload();
                    }
                },
                error: function (data) {
                    _showError(data);
                }
            });
        }       
    });
});