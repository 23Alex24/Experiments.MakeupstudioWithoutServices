﻿$(document).ready(function () {

    var $input = $("#photo");

    $input.fileinput({
        showPreview: false,
        showRemove: false,
        showUpload: false,
        browseLabel: "Выбрать"
    });
})