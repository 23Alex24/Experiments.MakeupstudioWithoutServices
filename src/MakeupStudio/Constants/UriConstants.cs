﻿namespace MakeupStudio.Constants
{
    public static class UriConstants
    {
        /// <summary>
        /// Главная страница админки
        /// </summary>
        public const string MAIN_PAGE = "/Home";        
    }
}