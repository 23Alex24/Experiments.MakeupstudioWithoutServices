﻿using MakeupStudio.Common.Extensions;
using MakeupStudio.Core;
using MakeupStudio.Core.Entities;
using MakeupStudio.Models.Orders;
using System.Threading.Tasks;

namespace MakeupStudio.ModelBuilders.Order
{
    /// <summary>
    /// Строит модель для редактирования заказа
    /// </summary>
    public class EditOrderModelBuilder
    {
        private readonly IUnitOfWork _unitOfWork;

        public EditOrderModelBuilder(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Строит модель для страницы редактирования заказа
        /// </summary>
        public async Task<EditOrderModel> Build(Account currentUser, long orderId)
        {
            var order = await _unitOfWork.OrderRepository.GetOrder(currentUser.CompanyId, orderId);

            if (order == null || order.OrderState != OrderState.Accepted)
                return null;

            var result = new EditOrderModel()
            {
                OrderId = orderId,
                AppointedTime = order.AppointedTime.Value.ToUtc(),
                CreatedDate = order.CreatedDate,
                ClientPhone = order.ClientPhone,
                ClientName = order.ClientName,
                ClientComment = order.ClientComment,
                MasterComment = order.MasterComment
            };

            return result;
        }
    }
}