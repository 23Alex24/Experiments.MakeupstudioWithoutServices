﻿using System;
using System.ComponentModel.DataAnnotations;
using MakeupStudio.Common.Constants;
using MakeupStudio.Core.Entities;

namespace MakeupStudio.Models.Orders
{
    /// <summary>
    /// Модель создания заказа
    /// </summary>
    public class CreateOrderModel
    {
        /// <summary>
        /// Имя клиента
        /// </summary>
        [Required(ErrorMessage = "Заполните имя клиента", AllowEmptyStrings = false)]
        [MaxLength(Company.NAME_MAX_LENGTH, ErrorMessage = "Слишком длинное имя")]
        [RegularExpression(RegexConstants.NAME, ErrorMessage = ValidationErrors.NAME_INCORRECT_FORMAT)]
        public string ClientName { get; set; }

        /// <summary>
        /// Телефон клиента
        /// </summary>
        [Required(ErrorMessage = "Укажите телефон клиента", AllowEmptyStrings = false)]
        [RegularExpression(RegexConstants.PHONE, ErrorMessage = ValidationErrors.PHONE_INCORRECT_FORMAT)]
        public string ClientPhone { get; set; }

        /// <summary>
        /// Время, на которое назначен прием для клиента (ко скольки клиент должен подойти) (в UTC)
        /// </summary>
        public DateTime AppointedTime { get; set; }

        /// <summary>
        /// Комментарий мастера
        /// </summary>
        [MaxLength(Order.MASTER_COMMENT_MAX_LENGTH, ErrorMessage = "Максимальная длина комментария {0} символов")]
        [RegularExpression(RegexConstants.COMMENT, ErrorMessage = ValidationErrors.COMMENT_INCORRECT_FORMAT)]
        public string MasterComment { get; set; }
    }
}