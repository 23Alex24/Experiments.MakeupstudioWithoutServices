﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using MakeupStudio.Common.Constants;

namespace MakeupStudio.Models.Account
{
    /// <summary>
    /// Модель для входа в админку
    /// </summary>
    public class LoginModel
    {
        [Required(ErrorMessage = ValidationErrors.REQUIRED)]
        [MinLength(LengthConstants.EMAIL_MIN, ErrorMessage = ValidationErrors.MIN_LENGTH)]
        [MaxLength(Core.Entities.Account.EMAIL_MAX_LENGTH, ErrorMessage = ValidationErrors.MAX_LENGTH)]
        [EmailAddress(ErrorMessage = ValidationErrors.EMAIL_INCORRECT_FORMAT)]
        [DisplayName("Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = ValidationErrors.REQUIRED)]
        [MinLength(LengthConstants.Password.MIN, ErrorMessage = ValidationErrors.MIN_LENGTH)]
        [MaxLength(LengthConstants.Password.MAX, ErrorMessage = ValidationErrors.MAX_LENGTH)]
        [RegularExpression(RegexConstants.PASSWORD, ErrorMessage = ValidationErrors.PASSWORD_INCORRECT_FORMAT)]
        [DisplayName("Пароль")]
        public string Password { get; set; }
    }
}