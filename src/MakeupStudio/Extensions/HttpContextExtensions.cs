﻿using System;
using System.Web;

namespace MakeupStudio.Extensions
{
    public static class HttpContextExtensions
    {
        /// <summary>
        /// Получает строковое значение из куков
        /// </summary>
        public static string GetStringClaimsValue(this HttpContextBase context, string claimsType)
        {
            string result = null;
            var claimsPrincipal = context.GetOwinContext()?.Authentication?.User;

            if (claimsPrincipal != null)
            {
                var idClaim = claimsPrincipal.FindFirst(claimsType);
                result = idClaim?.Value;
            }

            return result;
        }

        /// <summary>
        /// Получает числовое значение из куков
        /// </summary>
        public static int? GetIntClaimsValue(this HttpContextBase context, string claimsType)
        {
            var stringValue = context.GetStringClaimsValue(claimsType);

            if (string.IsNullOrWhiteSpace(stringValue))
                return null;

            int result;
            if (int.TryParse(stringValue, out result))
                return result;

            return null;
        }

        /// <summary>
        /// Получает Guid значение из куков
        /// </summary>
        public static Guid? GetGuidClaimsValue(this HttpContextBase context, string claimsType)
        {
            var stringValue = context.GetStringClaimsValue(claimsType);

            if (string.IsNullOrWhiteSpace(stringValue))
                return null;

            Guid result;
            if (Guid.TryParse(stringValue, out result))
                return result;

            return null;
        }

        /// <summary>
        /// Получает Enum значение из куков
        /// </summary>
        /// <typeparam name="T">Тип перечисления</typeparam>
        public static T? GetEnumValue<T>(this HttpContextBase context, string claimsType)
             where T : struct 
        {
            var stringValue = context.GetStringClaimsValue(claimsType);

            if (string.IsNullOrWhiteSpace(stringValue))
                return null;

            T result;
            if (Enum.TryParse(stringValue, out result))
                return result;

            return null;
        }
    }
}