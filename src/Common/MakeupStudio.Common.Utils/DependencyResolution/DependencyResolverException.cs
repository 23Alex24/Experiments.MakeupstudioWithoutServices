﻿using System;

namespace MakeupStudio.Common.Utils.DependencyResolution
{
    /// <summary>
    /// Исключение резолвера зависимостей
    /// </summary>
    public sealed class DependencyResolverException : Exception
    {
        public DependencyResolverException()
        {
        }

        public DependencyResolverException(string message) : base(message)
        {
        }

        public DependencyResolverException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
