﻿using System.Security.Cryptography.X509Certificates;

namespace MakeupStudio.Business.Errors
{
    /// <summary>
    /// Константы с ошибками
    /// </summary>
    public static class ErrorConstants
    {
        /// <summary>
        /// Системные ошибки
        /// </summary>
        public static class System
        {
            public const string INVALID_REQUEST = "Неверный запрос";
            public const string ARGUMENT_IS_NULL = "Argument is null";
            public const string INTERNAL_SERVER_ERROR = "Внутренняя ошибка сервера :( Повторите попытку позже.";
            public const string ACCESS_DENIED = "Доступ запрещен";
        }

        /// <summary>
        /// Ошибки по аккаунтам
        /// </summary>
        public static class Accounts
        {
            public const string INVALID_LOGIN_OR_PASS = "Неверный логин или пароль!";
            public const string GEOLOCATION_ERROR = "Введите существующий адрес";
            public const string EMAIL_ALREADY_USED = "Электронный адрес уже занят, необходимо использовать другой!";
            public const string PHONE_ALREADY_USED = "Телефон уже занят, необходимо использовать другой!";
            public const string ACCOUNT_NOT_VERIFIED = "Аккаунт еще не подтвержден! Повторите попытку позже.";
            public const string INVALID_ACCOUNT_STATE = "Доступ заблокирован";
        }

        /// <summary>
        /// Ошибки по фирме
        /// </summary>
        public static class Companies
        {
            public const string INVALID_COMPANY_STATE = "Доступ заблокирован";
            public const string NOT_FOUND = "Фирма не найдена";
        }

        /// <summary>
        /// Ошибки по услугам
        /// </summary>
        public static class Works
        {
            public const string ALREADY_EXISTS = "У Вас уже есть услуга с таким названием в данной категории";
            public const string CATEGORY_NOT_FOUND = "Категория не найдена";
            public const string NOT_FOUND = "Услуга не найдена";
        }

        public static class Photos
        {
            public const string MAX_COUNT = "Нельзя добавить больше фотографий. Удалите одну из существующих для добавления новой.";
            public const string NOT_FOUND = "Фотография не найдена";
        }

        public static class Orders
        {
            /// <summary>
            /// Когда пытаются повторно мастеру отправить заявку
            /// </summary>
            public const string BID_ALREADY_EXISTS = "Заявка уже отправлена, ожидайте, Вам перезвонят";
            public const string ORDER_NOT_FOUND = "Заказ не найден";

            public const string BID_NOT_FOUND = "Заявка не найдена";

            public const string HAS_ANOTHER_ORDERS = "Нельзя назначить прием на это время. У Вас назначен другой заказ в это время или рядом с ним";
        }
    }
}
