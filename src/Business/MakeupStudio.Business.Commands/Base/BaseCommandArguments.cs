﻿using MakeupStudio.Core.Entities;

namespace MakeupStudio.Business.Commands
{
    /// <summary>
    /// Базовый класс аргументов команды
    /// </summary>
    public abstract class BaseCommandArguments : ICommandArguments
    {
        /// <summary>
        /// Основная информация о текущем пользователе
        /// </summary>
        public virtual Account CurrentUser { get; set; }
    }
}
