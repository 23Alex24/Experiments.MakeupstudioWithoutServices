﻿namespace MakeupStudio.Business.Commands
{
    /// <summary>
    /// Пустой результат команды, в котором только базовые свойства
    /// </summary>
    public class EmptyCommandResult : BaseCommandResult
    {
    }
}
