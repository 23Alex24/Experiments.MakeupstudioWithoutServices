﻿using System;
using System.Text;
using System.Threading.Tasks;
using MakeupStudio.Common.Utils.Logger;

namespace MakeupStudio.Business.Commands
{
    /// <summary>
    /// Базовый класс команды
    /// </summary>
    /// <typeparam name="TArguments">Тип входных аргументов для команды</typeparam>
    /// <typeparam name="TResult">Результирующий тип команды</typeparam>    
    public abstract class BaseCommand<TArguments, TResult> : ICommand<TArguments, TResult>
            where TArguments : class, ICommandArguments
            where TResult : class, ICommandResult, new()
    {
        private readonly ILoggerUtility _loggerUtility;

        protected BaseCommand(ILoggerUtility loggerUtility)
        {
            _loggerUtility = loggerUtility;
        }

        protected virtual ILoggerUtility LoggerUtility
        {
            get { return _loggerUtility; }
        }

        /// <summary>
        /// Выполняет команду
        /// </summary>
        /// <param name="arguments">Входные параметры</param>
        public virtual async Task<TResult> Execute(TArguments arguments)
        {
            var type = this.GetType().ToString();

            try
            {
                LoggerUtility.Trace(GetLogMessage($"Command {type}. Run validation method.", arguments));

                var validateResult = await Validate(arguments);

                if (validateResult.IsSuccess)
                {
                    LoggerUtility.Trace(GetLogMessage($"Command {type}. Success validated.", arguments));
                    LoggerUtility.Trace(GetLogMessage($"Command {type}. Run perform method.", arguments));

                    var performResult = await InternalExecute(arguments);
                    if (performResult.IsSuccess)
                    {
                        LoggerUtility.Trace(GetLogMessage($"Command {type}. Success performed.", arguments));
                    }
                    else
                    {
                        var errorMsg = $"Command {type} perform failed. ErrorMsg = {performResult.ErrorMessage}";
                        LoggerUtility.Error(GetLogMessage(errorMsg, arguments));
                    }

                    return performResult;
                }

                var msg = $"Command {type} validation failed." +
                          $"ErrorMsg = {validateResult.ErrorMessage};" +
                          $"ErrorCode = {validateResult.ErrorCode}";
                LoggerUtility.Warn(GetLogMessage(msg, arguments));
                return validateResult;
            }
            catch (Exception ex)
            {
                var msg = GetLogMessage($"Unhandled exception in {type} command");
                LoggerUtility.Error(GetLogMessage(msg, arguments), ex);

                return new TResult()
                {
                    IsSuccess = false,
                    ErrorCode = 500,
                    ErrorMessage = "Internal server error"
                };
            }
        }

        /// <summary>
        /// Данный метод отвечает за выполнение команды
        /// </summary>
        protected abstract Task<TResult> InternalExecute(TArguments arguments);

        /// <summary>
        /// В данном методе должна происходить вся валидация и 
        /// различные проверки перед выполнением
        /// </summary>
        protected abstract Task<TResult> Validate(TArguments arguments);

        /// <summary>
        /// Добавляет к сообщению информацию о текущем классе
        /// </summary>
        /// <param name="msg">Сообщение</param>
        /// <param name="arguments">аргументы команды</param>
        /// <returns></returns>
        protected virtual string GetLogMessage(string msg, TArguments arguments = null)
        {
            var logMsg = new StringBuilder(msg);
            logMsg.Append("; Type = ");
            logMsg.Append(GetType());
            return logMsg.ToString();
        }
    }
}
