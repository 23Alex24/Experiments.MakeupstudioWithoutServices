﻿namespace MakeupStudio.Business.Commands
{
    /// <summary>
    /// Аргументы для команды удаления заказа
    /// </summary>
    public class DeleteOrderCommandArgs : BaseCommandArguments
    {
        /// <summary>
        /// Id заказа
        /// </summary>
        public int OrderId { get; set; }
    }
}
