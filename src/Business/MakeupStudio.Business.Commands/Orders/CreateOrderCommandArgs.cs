﻿using System;

namespace MakeupStudio.Business.Commands
{
    /// <summary>
    /// Аргументы для команды создания заказа
    /// </summary>
    public class CreateOrderCommandArgs : BaseCommandArguments
    {
        /// <summary>
        /// Имя клиента
        /// </summary>       
        public string ClientName { get; set; }

        /// <summary>
        /// Телефон клиента
        /// </summary>
        public string ClientPhone { get; set; }

        /// <summary>
        /// Время, на которое назначен прием для клиента (ко скольки клиент должен подойти) (в UTC)
        /// </summary>
        public DateTime AppointedTime { get; set; }

        /// <summary>
        /// Комментарий мастера
        /// </summary>
        public string MasterComment { get; set; }
    }
}
