﻿using System;

namespace MakeupStudio.Business.Commands
{
    /// <summary>
    /// Аргументы для команды создания новой заявки
    /// </summary>
    public class CreateBidCommandArgs : ICommandArguments
    {
        /// <summary>
        /// Id фирмы
        /// </summary>
        public Guid CompanyId { get; set; }

        /// <summary>
        /// Имя клиента
        /// </summary>
        public string ClientName { get; set; }

        /// <summary>
        /// Телефон клиента
        /// </summary>
        public string ClientPhone { get; set; }

        /// <summary>
        /// Комментарий клиента к заявке
        /// </summary>
        public string ClientComment { get; set; }
    }
}
