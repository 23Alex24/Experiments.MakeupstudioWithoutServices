﻿using System;
using System.Threading.Tasks;
using MakeupStudio.Common.Utils.Logger;
using MakeupStudio.Core.Entities;
using MakeupStudio.Core;

namespace MakeupStudio.Business.Commands
{
    /// <summary>
    /// Команда создания заказа
    /// </summary>
    internal class CreateOrderCommand : BaseCommand<CreateOrderCommandArgs, EmptyCommandResult>
    {
        private readonly IUnitOfWork _unitOfWork;

        public CreateOrderCommand(ILoggerUtility loggerUtility, IUnitOfWork unitOfWork) : base(loggerUtility)
        {
            _unitOfWork = unitOfWork;
        }

        protected override async Task<EmptyCommandResult> InternalExecute(CreateOrderCommandArgs arguments)
        {
            var result = new EmptyCommandResult() { IsSuccess = true };

            var order = new Order()
            {
                CompanyId = arguments.CurrentUser.CompanyId,
                AppointedTime = arguments.AppointedTime,
                ClientName = arguments.ClientName,
                ClientPhone = arguments.ClientPhone,
                CreatedDate = DateTime.UtcNow,
                MasterComment = arguments.MasterComment,
                OrderState = OrderState.Accepted
            };

            _unitOfWork.OrderRepository.Add(order);
            await _unitOfWork.SaveChangesAsync();

            return result;
        }

        protected override async Task<EmptyCommandResult> Validate(CreateOrderCommandArgs arguments)
        {
            var result = new EmptyCommandResult() { IsSuccess = true };

            //Проверяем что пользователь и фирма не заблокированы
            //
            var isActive = await _unitOfWork.AccountRepository.UserAndCompanyIsActive(
                arguments.CurrentUser.CompanyId, arguments.CurrentUser.Id);

            if (!isActive)
                return result.AddError(Errors.ErrorConstants.System.ACCESS_DENIED, 100);
            
            //Проверяем можно ли создать заказ на указанное время
            //
            var canCreate = await _unitOfWork.OrderRepository.CanChangeAppointedTime(
                arguments.CurrentUser.CompanyId, 0, arguments.AppointedTime);

            if (!canCreate)
                return result.AddError(Errors.ErrorConstants.Orders.HAS_ANOTHER_ORDERS, 1000);

            return result;
        }
    }
}
