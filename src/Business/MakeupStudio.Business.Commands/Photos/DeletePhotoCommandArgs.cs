﻿namespace MakeupStudio.Business.Commands.Photos
{
    /// <summary>
    /// Аргументы для команды удаления фотографии
    /// </summary>
    public class DeletePhotoCommandArgs : BaseCommandArguments
    {
        /// <summary>
        /// Id фотографии, которую нужно удалить
        /// </summary>
        public int PhotoId { get; set; }
    }
}
