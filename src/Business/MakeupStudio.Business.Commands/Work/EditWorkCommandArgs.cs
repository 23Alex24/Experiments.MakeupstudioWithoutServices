﻿namespace MakeupStudio.Business.Commands.Work
{
    /// <summary>
    /// Аргументы для команды изменения услуги
    /// </summary>
    public class EditWorkCommandArgs : BaseCommandArguments
    {
        /// <summary>
        /// Id услуги
        /// </summary>
        public int WorkId { get; set; }

        /// <summary>
        /// Название услуги
        /// </summary>
        public string WorkName { get; set; }

        /// <summary>
        /// Стоимость услуги
        /// </summary>
        public decimal Price { get; set; }
    }
}
