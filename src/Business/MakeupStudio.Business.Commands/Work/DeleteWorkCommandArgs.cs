﻿namespace MakeupStudio.Business.Commands.Work
{
    /// <summary>
    /// Аргументы команды удаления услуги
    /// </summary>
    public class DeleteWorkCommandArgs : BaseCommandArguments
    {
        public int WorkId { get; set; }
    }
}
