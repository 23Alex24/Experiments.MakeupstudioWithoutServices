﻿using System;
using System.Threading.Tasks;
using MakeupStudio.Core.Entities;

namespace MakeupStudio.Core
{
    /// <summary>
    /// Репозиторий для работы с сущностью фотографии
    /// </summary>
    public interface IPhotoRepository : IRepository<Photo>
    {
        /// <summary>
        /// Возвращает аватарку фирмы
        /// </summary>
        Task<Photo> GetAvatar(Guid companyId);

        /// <summary>
        /// Возвращает фотографии фирмы
        /// </summary>
        /// <param name="companyId">Id компании</param>
        Task<Photo[]> GetCompanyPhotos(Guid companyId);

        /// <summary>
        /// Возвращает количество фотографий, которые есть у фирмы
        /// </summary>
        /// <param name="companyId"></param>
        Task<int> GetPhotosCount(Guid companyId);

        /// <summary>
        /// Возвращает фотографию фирмы
        /// </summary>
        Task<Photo> GetPhoto(Guid companyId, int photoId);

        /// <summary>
        /// Возвращает фотографии фирм
        /// </summary>
        /// <param name="companiesId">Id фирм</param>
        Task<Photo[]> GetCompaniesPhotos(Guid[] companiesId);
    }
}
