﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace MakeupStudio.Core.Entities
{
    /// <summary>
    /// Аккаунт для входа в админку
    /// </summary>
    [Table("Accounts")]
    public class Account : IEntity<int>
    {
        public const int EMAIL_MAX_LENGTH = 50;
        public const int PHONE_MAX_LENGTH = 20;
        public const int NAME_MAX_LENGTH = 50;
        public const int PASSWORD_HASH_MAX_LENGTH = 500;

        [Key]
        [Column("Id")]
        public int Id { get; set; }

        /// <summary>
        /// Id фирмы, в которой работает сотрудник
        /// </summary>
        [ForeignKey(nameof(Company))]
        [Column("CompanyId")]
        public Guid CompanyId { get; set; }

        [Required]
        [MaxLength(EMAIL_MAX_LENGTH)]
        [Column("Email")]
        public string Email { get; set; }

        [MaxLength(PHONE_MAX_LENGTH)]
        [Column("Phone")]
        public string Phone { get; set; }

        [MaxLength(NAME_MAX_LENGTH)]
        [Column("Name")]
        public string Name { get; set; }

        /// <summary>
        /// Хеш код пароля
        /// </summary>
        [Required]
        [MaxLength(PASSWORD_HASH_MAX_LENGTH)]
        [Column("PasswordHash")]
        public string PasswordHash { get; set; }

        /// <summary>
        /// Состояние аккаунта
        /// </summary>
        [Column("AccountState")]
        public AccountState AccountState { get; set; }



        #region Навигационные свойства

        /// <summary>
        /// Навигационное свойство. Фирма, в которой работает сотрудник
        /// </summary>
        [JsonIgnore]
        public virtual Company Company { get; set; }

        #endregion
    }
}
