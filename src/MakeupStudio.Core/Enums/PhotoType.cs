﻿namespace MakeupStudio.Core.Entities
{
    /// <summary>
    /// Тип фотографии
    /// </summary>
    public enum PhotoType
    {
        /// <summary>
        /// Фотография альбома
        /// </summary>
        Album = 0,

        /// <summary>
        /// Фотография аватарки
        /// </summary>
        Avatar = 1
    }
}
