﻿namespace MakeupStudio.Core.Entities
{
    /// <summary>
    /// Состояние аккаунта
    /// </summary>
    public enum AccountState
    {
        /// <summary>
        /// Статус пользователя не установлен (используется при отсутствии значения статуса)
        /// </summary>
        Undefined = 0,

        /// <summary>
        /// Не подтвержден
        /// </summary>
        NotConfirmed = 1,

        /// <summary>
        /// Активный
        /// </summary>
        Active = 2,

        /// <summary>
        /// Удаленный
        /// </summary>
        Deleted = 3
    }
}
