﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using MakeupStudio.Core.Entities;
using MakeupStudio.Core.Dto;
using MakeupStudio.Infrastructure.Core.Services.EF.Extensions.Queryable;
using MakeupStudio.Infrastructure.Core.Services.EF.UnitOfWork;
using MakeupStudio.Core;

namespace MakeupStudio.Infrastructure.Core.Services.EF
{
    internal class CompanyRepository : EfRepository<Company>, ICompanyRepository
    {
        public CompanyRepository(DbContext context) : base(context) { }



        #region SearchCompanies

        public async Task<CompanyPreviewDto[]> SearchCompanies(int[] categoriesId, decimal? minPrice, decimal? maxPrice)
        {
            //Получаем список активных фирм с услугами
            var companies = await _dbContext.Set<Company>().AsNoTracking()
                .FilterByState(CompanyState.Active)
                .Where(x => x.Works.Any())
                .Include(x=>x.Photos)
                .Select(x => new CompanyPreviewDto()
                {
                    Name = x.Name,
                    Id = x.Id
                })
                .ToArrayAsync();

            if (companies.Length < 1)
                return new CompanyPreviewDto[0];

            var companiesId = companies.Select(x => x.Id).ToArray();

            //Получаем услуги фирм
            var filteredCompaniesId = await _dbContext.Set<Work>().AsNoTracking()
                .FilterByCategories(categoriesId)
                .FilterByMinPrice(minPrice)
                .FilterByMaxPrice(maxPrice)
                .Where(x => companiesId.Contains(x.CompanyId))
                .Select(x => x.CompanyId)
                .Distinct()
                .ToArrayAsync();
            
            //Отсеиваем фирмы, у которых не найдено отфильтрованных услуг
            return companies.Where(x => filteredCompaniesId.Contains(x.Id)).ToArray();
        }

        #endregion



        public async Task<Company[]> GetActiveCompanies()
        {
            return await _dbContext.Set<Company>().AsNoTracking()
                .FilterByState(CompanyState.Active)
                .Where(x=>x.Works.Any())
                .ToArrayAsync();
        }

        public async Task<CompanyState?> GetCompanyState(Guid companyId)
        {
            return await _dbContext.Set<Company>().AsNoTracking()
                .Where(x => x.Id == companyId)
                .Select(x => x.CompanyState)
                .FirstOrDefaultAsync();

        }

        /// <summary>
        /// Получает фирму по ее айди
        /// </summary>
        public async Task<Company> GetCompany(Guid companyId)
        {
            return await _dbContext.Set<Company>().AsNoTracking()
                .Where(x => x.Id == companyId)
                .FirstOrDefaultAsync();
        }

        /// <summary>
        /// Проверяет существует ли фирма
        /// </summary>
        public async Task<bool> CompanyIsExists(Guid companyId)
        {
            return await _dbContext.Set<Company>().AsNoTracking()
                .Where(x => x.Id == companyId)
                .AnyAsync();
        }
    }
}
