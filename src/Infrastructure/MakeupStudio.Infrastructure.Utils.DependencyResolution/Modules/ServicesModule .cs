﻿using System.Data.Entity;
using Autofac;
using MakeupStudio.Core;
using MakeupStudio.Infrastructure.Core.Services.EF;
using MakeupStudio.Infrastructure.Core.Services.EF.UnitOfWork;
using Module = Autofac.Module;

namespace MakeupStudio.Infrastructure.Utils.DependencyResolution.Modules
{
    /// <summary>
    /// Модуль, в котором находятся DI настройки для сервисов, работающих с БД
    /// </summary>
    public class ServicesModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<MakeupStudioContext>().AsSelf().As<DbContext>().InstancePerRequest();
            builder.RegisterType<EfUnitOfWork>().As<IUnitOfWork>().InstancePerRequest();
        }      
    }
}
