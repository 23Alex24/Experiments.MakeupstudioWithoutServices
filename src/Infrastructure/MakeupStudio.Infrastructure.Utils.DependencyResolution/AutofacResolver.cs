﻿using System;
using Autofac;
using JetBrains.Annotations;
using MakeupStudio.Common.Utils.DependencyResolution;

namespace MakeupStudio.Infrastructure.Utils.DependencyResolution
{
    /// <summary>
    /// Реализация DI с использованием библитеки Autofac
    /// </summary>
    internal class AutofacResolver : IDependcyResolver
    {
        private bool _initialized = false;
        private IContainer _autofacContainer;

        /// <summary>
        /// Инициализирует резолвер
        /// </summary>
        public void Initialize([NotNull]IContainer container)
        {
            if (_initialized)
                throw new DependencyResolverException("Resolver already initialized");

            _autofacContainer = container;
            _initialized = true;
        }

        public T Resolve<T>()
        {
            if (!_initialized)
                throw new DependencyResolverException("Resolver not initialized");

            try
            {
                return _autofacContainer.Resolve<T>();
            }
            catch (Exception ex)
            {
                throw new DependencyResolverException("Resolve failed. See inner exception", ex);
            }
        }
    }
}
